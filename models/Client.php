<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;

class Client extends Model {

    public $clientType = 0;
    public $code = '';
    public $name = '';
    public $firstName = '';
    public $lastName = '';
    public $email = '';
    public $phone = '';
    public $systemType = 0;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'clientType', 'systemType'], 'required'],
            [['code', 'name', 'email', 'phone', 'firstName', 'lastName'], 'string'],
            [['clientType', 'systemType'], 'integer'],
            [['clientType', 'systemType'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'clientType' => 'Type of the client',
            'code' => 'Personal code',
            'name' => 'Company name',
            'firstName' => 'First name',
            'lastName' => 'Last name',
            'email' => 'E-mail',
            'phone' => 'Phone',
            'systemType' => 'Client’s relation to the vehicle',
        ];
    }

    public function loadTestData($idx = 0) {

        if($idx == 0) {
            $this->clientType = 2;
            $this->code = '11117403';
            $this->name = 'Mihkel Mardikas';
            $this->firstName = 'Mihkel';
            $this->lastName = 'Mardikas';
            $this->email = 'Mihkel.Mardikas@poosas.eu';
            $this->phone = '333222111';
            $this->systemType = 1;
        } else {
            $this->clientType = 2;
            $this->code = '11115555';
            $this->name = 'Paul Tamm';
            $this->firstName = 'Paul';
            $this->lastName = 'Tamm';
            $this->email = 'paul.tamm@poosas.eu';
            $this->phone = '';
            $this->systemType = 2;
        }
    }

    /**
     * Creates and populates a set of models.
     *
     * @param string $modelClass
     * @param array $multipleModels
     * @return array
     */
    public static function createMultiple($modelClass, $multipleModels = [])
    {
        $model    = new $modelClass;
        $formName = $model->formName();
        $post     = \Yii::$app->request->post($formName);
        $models   = [];

        if (! empty($multipleModels)) {
            $keys = array_keys(ArrayHelper::map($multipleModels, 'id', 'id'));
            $multipleModels = array_combine($keys, $multipleModels);
        }

        if ($post && is_array($post)) {
            foreach ($post as $i => $item) {
                if (isset($item['id']) && !empty($item['id']) && isset($multipleModels[$item['id']])) {
                    $models[] = $multipleModels[$item['id']];
                } else {
                    $models[] = new $modelClass;
                }
            }
        }

        unset($model, $formName, $post);

        return $models;
    }
}