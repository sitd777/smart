<?php

namespace app\models;

class Api {

    public $timeSessionKey = 'ApiLastUsed';

    public $timeoutSec = 60;

    public $url = 'http://test.smartkindlustus.ee/api/get_offer';

    public $user = 'smart';

    public $password = 'kindlustus';

    public $resultCode = 0;

    public $result = '';

    public $error = '';

    public $requestTime = 0;

    public function request($data) {
        // Check time of last usage od API
        $session = \Yii::$app->session;
        $time = $session->get($this->timeSessionKey, 0);
        if($time && $time > time() - $this->timeoutSec) {
            $this->error = 'Error: Too frequent usage of API. Last used: ' . date('d.m.Y H:i:s', $time) . '; Next usage after: ' . ($this->timeoutSec - (time() - $time)) . ' sec.';
            return 0;
        }

        $start = $this->microtime_float();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(['json' => json_encode($data)]));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $this->user.':'.$this->password);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json'
            //'Content-Type: application/json'
        ));

        $result = curl_exec($ch);
        $this->resultCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if($this->resultCode !== 200) {
            $this->error = 'Error: ' . $this->resultCode;
        } else if($result === false) {
            $this->error = 'Curl error: ' . curl_error($ch);
        } else {
            $this->result = json_decode($result);
        }
        curl_close($ch);

        // Calculate time of request
        $this->requestTime = $this->microtime_float() - $start;

        // Save time of last usage to session
        $session->set($this->timeSessionKey, time());

        return $this->resultCode;
    }

    protected function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
}