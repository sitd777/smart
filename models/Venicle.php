<?php

namespace app\models;

use yii\base\Model;

class Venicle extends Model {

    public $make = '';
    public $model = '';
    public $insuranceModel = '';
    public $vinCode = '';
    public $year = 0;
    public $power = 0;
    public $type = 0;
    public $usageType = 0;
    public $lease = 0;
    public $registrationCode = '';
    public $weight = 0.0;
    public $price = 0.0;
    public $registrationCertificateNumber = '';
    public $odometerDisplay = 0;
    public $numberOfKeys = 0;
    public $numberOfAlarmKeys = 0;
    public $numberOfDoors = 0;
    public $numberOfAirbags = 0;
    public $numberOfSeats = 0;
    public $bodyType = 0;
    public $fuelType = 0;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['make', 'model', 'year', 'vinCode', 'power'], 'required'],
            [['make', 'model', 'insuranceModel', 'vinCode', 'registrationCode', 'registrationCertificateNumber'], 'string'],
            [['year', 'power', 'type', 'usageType', 'lease', 'odometerDisplay', 'numberOfKeys', 'numberOfAlarmKeys', 'numberOfDoors', 'numberOfAirbags', 'numberOfSeats', 'bodyType', 'fuelType', 'power'], 'integer'],
            [['weight', 'price'], 'number'],
            [['year', 'power', 'type', 'usageType', 'lease', 'odometerDisplay', 'numberOfKeys', 'numberOfAlarmKeys', 'numberOfDoors', 'numberOfAirbags', 'numberOfSeats', 'bodyType', 'fuelType', 'weight', 'price', 'power'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'make' => 'Brand of the vehicle',
            'model' => 'Model of the vehicle',
            'insuranceModel' => 'Name of the insurance model',
            'vinCode' => 'VIN code',
            'year' => 'Year of the vehicle built',
            'power' => 'Power in KW',
            'type' => 'Vehicle type',
            'usageType' => 'Vehicle usage type',
            'lease' => 'Vehicle lease status',
            'registrationCode' => 'Registration number',
            'weight' => 'Weight',
            'price' => 'Price of the vehicle',
            'registrationCertificateNumber' => 'Registration certificate number',
            'odometerDisplay' => 'Odometer display value',
            'numberOfKeys' => 'Number of keys',
            'numberOfAlarmKeys' => 'Number of alarm keys',
            'numberOfDoors' => 'Number of doors',
            'numberOfAirbags' => 'Number of airbags',
            'numberOfSeats' => 'Number of seats',
            'bodyType' => 'Vehicle body type',
            'fuelType' => 'Vehicle fuel type',
        ];
    }

    public function loadTestData() {
        $this->make = 'Mercedes-Benz';
        $this->model = 'GL 350 BlueTEC 4MATIC';
        $this->year = 2015;
        $this->vinCode = 'VINCODE123456';

        $this->registrationCode = '777 ABC';
        $this->power = 7.0;
        $this->odometerDisplay = 123456;
        $this->numberOfKeys = 2;

        $this->numberOfAlarmKeys = 2;
        $this->numberOfDoors = 4;
        $this->numberOfAirbags = 7;
        $this->numberOfSeats = 5;

        $this->bodyType = 3;
        $this->fuelType = 1;
        $this->power = 130;
    }
}