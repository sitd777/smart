<?php

namespace app\controllers;

use app\models\Api;
use app\models\Client;
use app\models\Venicle;
use app\widgets\Flashes;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $request = \Yii::$app->request;
        $post = $request->post();
        $venicle = new Venicle();
        $clients = [new Client()];

        if ($venicle->load($post)) {

            $clients = Client::createMultiple(Client::classname());
            Client::loadMultiple($clients, $post);

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($clients),
                    ActiveForm::validate($venicle)
                );
            }

            // validate all models
            $valid = $venicle->validate();
            $valid = Client::validateMultiple($clients) && $valid;
            if ($valid) {
                // Cleanup objects
                $data = [
                    'vehicle' => (object)$venicle->getAttributes(),
                    'clients' => [],
                ];
                foreach($clients as $client) {
                    $data['clients'][] = (object)$client->getAttributes();
                }
                $data = ['data' => (object)$data];

                // Send data
                $api = new Api();
                if($api->request($data) == 200) {
                    $result = (array)$api->result;
                    if(strtolower($result['status']) == 'ok') {
                        Flashes::setSuccess('Success: <br/>' .
                            'Offergroup ID: ' . $result['Offergroup ID'] . '<br/>' .
                            'Company ID: ' . $result['company ID'] . '<br/>' .
                            'User ID: ' . $result['user ID'] . '<br/>' .
                            'Status: ' . $result['status'] . '<br/>' .
                            'Time: ' . number_format($api->requestTime, 3, '.', '') . ' sec.'
                        );
                    } else {
                        Flashes::setError($result['reason']);
                    }
                } else {
                    Flashes::setError($api->error);
                }
            }
        } else {
            if($request->get('test', 0)) {
                $venicle->loadTestData();
                $clients = [new Client(), new Client()];
                $clients[0]->loadTestData(0);
                $clients[1]->loadTestData(1);
            }
        }

        return $this->render('index', [
            'venicle' => $venicle,
            'clients' => $clients,
        ]);
    }
}
