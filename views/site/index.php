<?php
use \yii\helpers\Html;
use \yii\bootstrap\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

$form = ActiveForm::begin(['options' => ['autocomplete' => 'off'], 'id' => 'dynamic-form', 'action' => \yii\helpers\Url::to(['index'])]);

/**
 * @var \app\models\Venicle $venicle
 */

$bodyList = [ // -1
    1 => 'Convertible',
    2 => 'Coupe',
    3 => 'Hatchback',
    4 => 'Combi',
    5 => 'Limousine',
    6 => 'Pickup',
    7 => 'Sedan',
    8 => 'Truck',
    9 => 'Special',
    16 => 'Other',
];
$fuelList = [ // -1
    1 => 'CNG',
    2 => 'Diesel',
    3 => 'Electric',
    4 => 'Gas',
    5 => 'Hybrid',
    6 => 'LPG',
    7 => 'etrol',
    16 => 'Other',
];
$clientTypeList = [
    1 => "Physical entity",
    2 => "Legal entity",
];
$systemTypeList = [
    1 => "Owner",
    2 => "User",
    3 => "Lessee",
];
?>
<div class="box box-default color-palette-box">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-tag"></i> Get offer</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#venicle" data-toggle="tab">Venicle</a></li>
                        <li><a href="#clients" data-toggle="tab">Clients</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="venicle">

                            <div class="row">
                                <div class="col-md-3">
                                    <?= $form->field($venicle, 'make')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($venicle, 'model')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($venicle, 'year')->input('number') ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($venicle, 'vinCode')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <?= $form->field($venicle, 'registrationCode')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($venicle, 'power')->input('number') ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($venicle, 'odometerDisplay')->input('number') ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($venicle, 'numberOfKeys')->input('number') ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <?= $form->field($venicle, 'numberOfAlarmKeys')->input('number') ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($venicle, 'numberOfDoors')->input('number') ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($venicle, 'numberOfAirbags')->input('number') ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($venicle, 'numberOfSeats')->input('number') ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <?= $form->field($venicle, 'bodyType')->dropDownList([0 => '---'] + $bodyList) ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($venicle, 'fuelType')->dropDownList([0 => '---'] + $fuelList) ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $form->field($venicle, 'power')->input('number') ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="clients">
                            <?php DynamicFormWidget::begin([
                                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                'widgetBody' => '.container-items', // required: css class selector
                                'widgetItem' => '.item', // required: css class
                                'limit' => 4, // the maximum times, an element can be cloned (default 999)
                                'min' => 1, // 0 or 1 (default 1)
                                'insertButton' => '.add-item', // css class
                                'deleteButton' => '.remove-item', // css class
                                'model' => $clients[0],
                                'formId' => 'dynamic-form',
                                'formFields' => [
                                    'full_name',
                                    'address_line1',
                                    'address_line2',
                                    'city',
                                    'state',
                                    'postal_code',
                                ],
                            ]); ?>

                            <div class="container-items">
                                <?php foreach ($clients as $i => $modelClient): ?>
                                    <div class="item panel panel-default"><!-- widgetBody -->
                                        <div class="panel-heading">
                                            <h3 class="panel-title pull-left">Client</h3>
                                            <div class="pull-right">
                                                <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <?= $form->field($modelClient, "[{$i}]name")->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-sm-3">
                                                    <?= $form->field($modelClient, "[{$i}]code")->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-sm-3">
                                                    <?= $form->field($modelClient, "[{$i}]email")->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-sm-3">
                                                    <?= $form->field($modelClient, "[{$i}]phone")->textInput(['maxlength' => true]) ?>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <?= $form->field($modelClient, "[{$i}]clientType")->dropDownList([0 => '---'] + $clientTypeList) ?>
                                                </div>
                                                <div class="col-sm-3">
                                                    <?= $form->field($modelClient, "[{$i}]systemType")->dropDownList([0 => '---'] + $systemTypeList) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <?php DynamicFormWidget::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <?php echo Html::a('Load test data', ['index', 'test' => 1], ['class' => 'btn btn-warning']) ?>
            <?= Html::submitButton('Send data', ['class' => 'btn btn-success']) ?>
        </div>

    </div>
</div>

<?php ActiveForm::end(); ?>
