<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(Yii::$app->name . ($this->title ? ': ' . $this->title : '')) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    if(!Yii::$app->user->isGuest)  {
        $menuItems = [
            /*['label' => Lx::t('app', 'Закупки'), 'url' => ['/deals-in/index'], 'visible' => Yii::$app->user->can('metall/deals-in/index')],
            ['label' => Lx::t('app', 'Расчёты с поставщиками'), 'url' => ['/payments-in/index'], 'visible' => Yii::$app->user->can('metall/payments-in/index')],
            ['label' => Lx::t('app', 'Продажи'), 'url' => ['/deals-out/index'], 'visible' => Yii::$app->user->can('metall/deals-out/index')],
            ['label' => Lx::t('app', 'Расчёты с покупателями'), 'url' => ['/payments-out/index'], 'visible' => Yii::$app->user->can('metall/payments-out/index')],
            ['label' => Lx::t('app', 'Расходы'), 'url' => ['/costs/index'], 'visible' => Yii::$app->user->can('metall/costs/index')],
            ['label' => Lx::t('app', 'Tulud/kuulud'), 'url' => ['/s-costs/index'], 'visible' => Yii::$app->user->can('metall/s-costs/index')],
            [
                'label' => Lx::t('app', 'Билал'),
                'items' => [
                    ['label' => Lx::t('app', 'Билал - Кошельки'), 'url' => ['/bil-pockets/index'], 'visible' => Yii::$app->user->can('metall/bil-pockets/index')],
                    ['label' => Lx::t('app', 'Билал - Операции'), 'url' => ['/bil-operations/index'], 'visible' => Yii::$app->user->can('metall/bil-operations/index')],

                ],
                'visible' => Yii::$app->user->can('metall/bil/index'),
            ],
            [
                'label' => Lx::t('app', 'Дополнительно'),
                'items' => [
                    ['label' => Lx::t('app', 'Кошельки'), 'url' => ['/pockets/index'], 'visible' => Yii::$app->user->can('metall/pockets/index')],
                    ['label' => Lx::t('app', 'История кошельков'), 'url' => ['/pockets-history/index'], 'visible' => Yii::$app->user->can('metall/pockets-history/index')],
                    '<li class="divider"></li>',
                    ['label' => Lx::t('app', 'Обмен RUB/USD'), 'url' => ['/exchange/index'], 'visible' => Yii::$app->user->can('metall/exchange/index')],
                    '<li class="divider"></li>',
                    //['label' => Lx::t('app', 'Хедж'), 'url' => ['/hedge/index'], 'visible' => Yii::$app->user->can('metall/hedge/index')],
                    ['label' => Lx::t('app', 'Хедж - Пополнение'), 'url' => ['/hedge/index'], 'visible' => Yii::$app->user->can('metall/hedge/index')],
                    ['label' => Lx::t('app', 'Хедж - Остаток'), 'url' => ['/hedge-status/index'], 'visible' => Yii::$app->user->can('metall/hedge-status/index')],
                    ['label' => Lx::t('app', 'Хедж - Прибыль/Убыток'), 'url' => ['/hedge-income/index'], 'visible' => Yii::$app->user->can('metall/hedge-income/index')],
                    '<li class="divider"></li>',
                    ['label' => Lx::t('app', 'Инвестиции'), 'url' => ['/capital/index'], 'visible' => Yii::$app->user->can('metall/capital/index')],
                    ['label' => Lx::t('app', 'Вывод средств'), 'url' => ['/money-out/index'], 'visible' => Yii::$app->user->can('metall/money-out/index')],
                    '<li class="divider"></li>',
                    ['label' => Lx::t('app', 'Статистика'), 'url' => ['/stats/index'], 'visible' => Yii::$app->user->can('metall/stats/index')],
                    ['label' => Lx::t('app', 'Статистика главной страницы'), 'url' => ['/stats-index/index'], 'visible' => Yii::$app->user->can('metall/stats-index/index')],
                ],
                'visible' => Yii::$app->user->can('metall/additional/index'),
            ],
            [
                'label' => Lx::t('app', 'Настройки'),
                'items' => [
                    ['label' => Lx::t('app', 'Инвесторы'),  'url' => ['/investors/index'], 'visible' => Yii::$app->user->can('metall/investors/index') ],
                    ['label' => Lx::t('app', 'Поставщики'), 'url' => ['/providers/index'], 'visible' => Yii::$app->user->can('metall/providers/index') ],
                    ['label' => Lx::t('app', 'Покупатели'), 'url' => ['/buyers/index'], 'visible' => Yii::$app->user->can('metall/buyers/index') ],
                    ['label' => Lx::t('app', 'Материалы'),  'url' => ['/materials/index'], 'visible' => Yii::$app->user->can('metall/materials/index') ],
                    ['label' => Lx::t('app', 'Логистика'),  'url' => ['/logistics/index'], 'visible' => Yii::$app->user->can('metall/logistics/index') ],
                    ['label' => Lx::t('app', 'Типы расходов'),  'url' => ['/costs-types/index'], 'visible' => Yii::$app->user->can('metall/costs-types/index') ],
                    ['label' => Lx::t('app', 'Кошельки'),  'url' => ['/pockets-types/index'], 'visible' => Yii::$app->user->can('metall/pockets-types/index') ],
                    ['label' => Lx::t('app', 'Хедж - Типы'),  'url' => ['/hedge-types/index'], 'visible' => Yii::$app->user->can('metall/hedge-types/index') ],
                    ['label' => Lx::t('app', 'Хедж - Источники'),  'url' => ['/hedge-sources/index'], 'visible' => Yii::$app->user->can('metall/hedge-sources/index') ],
                    ['label' => Lx::t('app', 'Типы доходов/расходов'),  'url' => ['/s-costs-types/index'], 'visible' => Yii::$app->user->can('metall/s-costs-types/index') ],
                    //'<li class="divider"></li>',
                    ['label' => Lx::t('app', 'Пользователи'),  'url' => ['/users/index'], 'visible' => Yii::$app->user->can('system/users/index') ],
                    ['label' => Lx::t('app', 'Группы'),  'url' => ['/system/groups/index'], 'visible' => Yii::$app->user->can('system/groups/index') ],

                ],
                'visible' => Yii::$app->user->can('metall/options/index'),
            ],
            '<li>'
            . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link']
            )
            . Html::endForm()
            . '</li>'*/
        ];
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items'   => $menuItems,
        ]);
    }

    NavBar::end();
    ?>



    <div class="container">
        <section class="breadcrumbs">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </section>

        <section class="messages">
            <?php echo \app\widgets\Flashes::widget() ?>
        </section>

        <section class="content">
            <?= $content ?>
        </section>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
