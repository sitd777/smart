<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class FontIoniconsAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/fonts/ionicons-2.0.1/';
    public $css = [
        'css/ionicons.min.css',
    ];
    public $js = [
    ];
    public $depends = [
    ];
}
